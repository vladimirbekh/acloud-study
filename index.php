<?php
/**
 * include array $data
 * @var $data array
 */
include('./data.php');
$hash = rand(1, 1000);
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Урок верстки</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="./css/vars.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/main.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/animation.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/buttons.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/question.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/main-block.css?hash=<?=$hash?>">
        <link rel="stylesheet" href="./css/faq.css?hash=<?=$hash?>">
    </head>
    <body>
        <div class="main-block">
            <div class="main-block__inner">
                <div class="faq">
                    <div class="faq__preview">
                        <div class="faq__title title main-block__title">
                            <?=$data['FAQ']['title']?>
                        </div>
                        <div class="faq__text color-text font-text__large">
                            <?=$data['FAQ']['description']?>
                        </div>
                        <div class="faq__btn btn btn__lg">
                            <?=$data['FAQ']['button_text']?>
                        </div>
                    </div>

                    <div class="faq__items">
                        <?php foreach($data['FAQ']['items'] as $item):?>
                            <div class="faq__question question radius" data-collapse>
                                <div class="question__preview">
                                    <div class="question__title title">
                                        <?=$item['title']?>
                                    </div>
                                    <div class="question__plus"></div>
                                </div>

                                <div class="question__detail" data-collapse-target>
                                    <div class="question__detail-text color-text font-text__large">
                                        <?=$item['text']?>
                                    </div>
                                    <div class="question_button btn btn__primary">
                                        <?=$item['button_text']?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>


        <script src="./js/animation.js?hash=<?=$hash?>"></script>
    </body>
</html>
