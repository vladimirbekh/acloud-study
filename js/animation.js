function collapse()
{
    /* example on JQuery
    $(document).on('click', '[data-collapse]', function() {
        this.classToggle('show');
    });
     */

    const collapse_triggers = document.querySelectorAll('[data-collapse]');
    collapse_triggers.forEach(trigger => {
        trigger.addEventListener('click', function() {
            this.classList.toggle('show');
        });
    });

    const collapse_targets = document.querySelectorAll('[data-collapse-target]');
    collapse_targets.forEach(target => {
        target.addEventListener('click', function(event) {
            event.stopPropagation();
        });
    });
}

collapse();